﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA
{
    /// <summary>
    /// 設定無論何種敵人的狀態機 例如: 近戰判定 , 遠程攻擊判定 ,追擊判定
    /// 不需要設定數值
    /// </summary>
    public class AI_States : MonoBehaviour
    {



        public GameObject UChan;
        public AI_data data;
        Animator anim;
        string animation;

       [Header("偵測當前狀態")]
        public bool isMelleAttack; //是否為近戰攻擊中
        public bool isChase; //是否為追擊狀態中
        public bool isSeek; //是否為追蹤敵人中
        public float distance;
        
        public string[] MelleSkillAnim;
        void Start()
        {

            anim = GetComponent<Animator>();
            data = GetComponent<AI_data>(); 
            UChan = GameObject.Find("Controller");
            //anim = GetComponent<Animator>();
            StartCoroutine(AttackTime());
        }

        // Update is called once per frame
        void Update()
        {
            
            
            
        }
        public void DistanceOfTarget() // 計算與主角的距離
        {
            distance = Vector3.Distance(this.transform.position, UChan.transform.position);
        }
        public void FindTarget() //找到主角的相關動作
        {
            
        }

        public void MelleAttack() // 普通攻擊
        {
            
            string animation = null;
            int attack = Random.Range(0, MelleSkillAnim.Length);
            if (distance <= data.attackDistance)
            {
                animation = MelleSkillAnim[attack]; //隨機普通近戰攻擊
                isMelleAttack = true;
                print(animation);
            }
            else
            {
                isMelleAttack = false;
            }
            anim.CrossFade(animation, 0.1f);
        }
        public void CountTime()
        {
           //float StartCount += Time.deltaTime; 
        }
        IEnumerator AttackTime()
        {
            //for ( float i =0; i<= 5; i += Time.deltaTime )
            //{
            //    print( i + "second");
            //    yield return new WaitForSeconds(3); 
            //}
            
                print("second");
                yield return new WaitForSeconds(3);
            
        }
    }

}
