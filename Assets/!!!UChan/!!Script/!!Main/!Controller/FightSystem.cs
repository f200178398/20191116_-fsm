﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    
    public class FightSystem : MonoBehaviour
    {
        [Header("主角狀態")]
        public GameObject Controller;
        public float controllerHp;
        
        public float damage = 200f;
        public int[] damages;
        public float machineDamage;

        public StateManager state;
        public EnemyTarget enemyTarget;//敵人狀態
        public OnTrigger onTrigger;
        [Header("敵人資訊")]
        public Collider enemyCol;
        public GameObject enemyObj; //ray
        public float enemyHp;
        

        [Header("攻擊中")]
        public bool attack;

        [Header("被敵人擊中")]
        public bool underAttack;

        //public Collider col;
        [Header("武器資訊")]
        public GameObject weapeon;
        public Collider weapCol; //武器碰撞

        
        [HideInInspector]
        public RaycastHit targetHit;
        public Ray controllerRay;




        [Header("Enemy_data")]
        public AI_data ai_data;
        private float minHp = 0;

        public GameObject[] enemys;
        public float enemysHp;
        public int enemysCount;
        public List<Collider> enemyList;
        
        // Start is called before the first frame update
        void Start()
        {
            ///// ********** 如果只有在Start 呼叫其他程式的資料 ,  那事件觸發之後的敵人就可能無法參考???  有待確認
           
            

            state = GetComponent<StateManager>();
            weapeon = GameObject.Find("Weapon_point");  //先抓到主物件以外的武器物件
            //print(weapeon.name);
            weapeon.SetActive(false); // 一開始武器不在手上
            weapCol = weapeon.GetComponent<Collider>();  //先存入武器的碰撞器
            weapCol.enabled = !enabled;  //武器碰撞先關閉
            //Debug.Log(StateManager.Instance().onGround);
            
            Controller = GameObject.Find("Controller");
            //enemyHp = AI_data.currentHp;
            //enemyData = GameObject.FindGameObjectWithTag("Enemy").GetComponent<AI_data>();
            //enemyData = GameObject.FindWithTag("Enemy").GetComponent<AI_data>();
            enemys = GameObject.FindGameObjectsWithTag("Enemy"); //將所有的"Enemy" 存入"陣列"
            

            enemysCount = GameObject.FindGameObjectsWithTag("Enemy").Length; //查找所有的敵人數量

            enemyTarget = GameObject.FindGameObjectWithTag("Enemy").GetComponent<EnemyTarget>();

            onTrigger = GameObject.Find("Controller").GetComponent<OnTrigger>(); //觸發被敵方攻擊的碰撞體
            //Debug.Log(enemyTarget.currentHp); 確實顯示起始血量
            //Collider enemyCol = GameObject.FindGameObjectsWithTag
            // ///嘗試使用LIST  是否能在遊戲中擴充敵人數量 

            // for(int i = 0; i<enemysCount; i++)
            // {
            //     print(enemys[i]); //印出場景所有敵人名字
            // }

            //print(enemysCount);//印出敵人數量
            //enemyCol.gameObject.GetComponent<AI_data>();
            
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            
            //Attackking();
        }
        void Update()
        {
            enemysHp = enemyTarget.currentHp; //將當前目標的血量存入
            Controller_Ray();
            //Debug.Log(enemyCol);
            // DamageEnabled();
            print(underAttack); //尚未觸發
            //Debug.Log(enemyObj);
            TakeDamage();
            
        }
        
        public void TakeDamage()  // 受到傷害 ///尚未觸發
        {
            /// 傷害職必須從AI_data呼叫
            /// 
            //underAttack = onTrigger.underAttack;

            //if ( enemyTarget.attack == true)
            //{
            //    print("UUUUU");
            //}

            // // 觸發敵人的武器
            //if (underAttack == true )  ///遭受敵人攻擊,
            //{
               // controllerHp -= ai_data.melleAttackD_Damage; //暫時沒效果
               // print(controllerHp);
                if (controllerHp <= 0) //由這裡判定主角死亡
                {
                    controllerHp = 0;
                    print("you are dead !!"); // 正在執行
                }
            //}
        }
                
        public void Attackking() //攻擊給予傷害  ** 確實對EnemyTarget執行
        {
            attack = true;
            print("命中敵人"); //確實執行
            //StateManager AttackType_A = new StateManager();
            //AI_data enemyData = new AI_data();
            //if (StateManager.Instance().rt || StateManager.Instance().lt) // 似乎不需要依賴按鍵的觸發 , 全靠武器的碰撞開關
            //{
                //enemysHp -= damage;
               
            // print("揮動武器");  //
            //for(int i = 0 ; i <= damages.Length; i++)
            //{ 


            //enemyHp -= damage;




            //print(""damage); // damage 被認為是0  在面板修改 
                //print(enemyHp);
                //Debug.Log("damage");
                //}
                //Debug.Log(AttackType_A);
                //enemyData.currentHp -= damage;
                //Debug.Log(enemyData.currentHp);
                
            //}
            //else
            //{
            //    attack = false;

            //}
           

            


            // print(enemyData.maxHp);
            //print(enemyData.minHp);

        }
        public void Controller_Ray() //抓到 角色前方的敵人COllider   
        {
            controllerRay = new Ray(Controller.transform.position, Vector3.forward); //指定 腳色 RAY位置
            if (Physics.Raycast(controllerRay, out targetHit, 30f)) //RAY 偵測並抓到敵
            {
                if (targetHit.collider.tag == "Enemy")
                {
                    enemyCol = targetHit.collider;
                    enemyObj = enemyCol.gameObject;
                }
            }
            else  //當RAY 沒抓到敵人時返回NULL
            {
                enemyObj = null;
                enemyCol = null;
            }

        }
        
        public void Dead()
        {
            if(enemyHp <= 0 )
            {
                enemyHp = minHp;
                Debug.Log("Dead!");
            }
        }
        
    }

}
