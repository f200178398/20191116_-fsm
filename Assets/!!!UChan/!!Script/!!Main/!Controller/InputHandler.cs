﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SA
{
    public class InputHandler : MonoBehaviour
    {
        float vertical;
        float horizontal;
        float jump;
        bool runInput;

        bool b_input;
        bool a_input;
        
        bool x_input;
        bool y_input;

        bool rb_input;
        float rt_axis;
        bool rt_input;
        bool lb_input;
        float lt_axis;
        bool lt_input;
        float jump_axis;
        bool jump_input;

        bool leftAxis_down;
        bool rigtAxis_down;
       

        StateManager states;
        CameraManager camManager;
        
        
        float delta;

        
        void Start()
        {
            states = GetComponent<StateManager>();
            states.Init();

            camManager = CameraManager.singleton;
            camManager.Init(this.transform);
            //FollowMachine.FollowMachine_2 FM = new FollowMachine.FollowMachine_2();
        }

        
        void FixedUpdate()
        {
            delta = Time.fixedDeltaTime;
            states.FixedTick(delta);
            GetInput();
            UpdateStates();
           

            //Debug.Log("FixedUpdate time :" + delta);
            camManager.Tick(delta);
        }
        void Update()
        {
            delta = Time.deltaTime;
           // states.FixedTick(delta);
            states.Tick(delta);
            UpdateStates();
        }
        void GetInput()
        {
            vertical = Input.GetAxis("Vertical");
            horizontal = Input.GetAxis("Horizontal");
            b_input = Input.GetButton("b_input"); //跑
            a_input = Input.GetButton("A"); // 跟隨機
            
            x_input = Input.GetButton("X");
            y_input = Input.GetButton("Y");
            rt_axis = Input.GetAxis("RT");
            rt_input = Input.GetButtonDown("RT");
            if (rt_axis != 0)
                rt_input = true;

            jump_axis = Input.GetAxis("Jump");
            jump_input = Input.GetButton("Jump");
            if (jump_axis != 0)
                jump_input = true;

            lt_input = Input.GetButtonDown("LT");
            lt_axis = Input.GetAxis("LT");
            if (lt_axis != 0)
                lt_input = true;
            rb_input = Input.GetButtonDown("RB");
            lb_input = Input.GetButton("LB");

            rigtAxis_down = Input.GetButton("L"); //鎖定


        }
        void UpdateStates()
        {
            states.horizontal = horizontal;
            states.vertical = vertical;
            //states.jump = jump;
            Vector3 v = vertical * camManager.transform.forward;
            Vector3 h = horizontal * camManager.transform.right;
            //Vector3 j = jump * camManager.transform.up;
            states.moveDir = (v + h).normalized;
            float m = Mathf.Abs(horizontal) + Mathf.Abs(vertical);

          // states.turnMoveAmount = 0.3f * horizontal;
            ///
            if (vertical != 0)
            {
                float RR = (Mathf.Sin(-vertical)) * states.TemP_angle * Mathf.Deg2Rad;
                states.turnMoveAmount = RR; //Turn數值**input  動畫用///////////*********8
                print("vertical!=0 等於" + vertical);
            }
            else
            {
                float RR = horizontal;
                states.turnMoveAmount = horizontal; //Turn數值**input  動畫用///////////*********8
                vertical = 0;
            }
            ///
            states.moveAmount = Mathf.Clamp01(m);

            if (b_input)
            {
                states.run = (states.moveAmount > 0);
                   
            }
            else
            {
                states.run = false;
            }
            states.jum = jump_input;
            states.rb = rb_input;
            states.rt = rt_input;
            states.lb = lb_input;
            states.lt = lt_input;
            states.Y = y_input; //換武器 姿勢
            states.A = a_input;

            if (y_input)
            {
                states.isTwoHanded = !states.isTwoHanded;
                states.HandleTwoWeapon();
            }
            if (rigtAxis_down)
            {
                states.lockOn = !states.lockOn;
                if(states.lockOnTarget == null)
                    states.lockOn = false;
                camManager.lockonTarget = states.lockOnTarget.transform;
                camManager.lockon =  states.lockOn;

                //Debug.Log("lock on target");
                //states.lockOn = true;// 目標鎖定後的bool保持

               // Debug.Log(camManager.lockonTarget);
            }
            
        }

    }
}

